/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package bancobcp.devolverbilletes;

import java.util.Scanner;

/**
 *
 * @author Eduardo Durand <eduardo.durand.c@uni.pe>
 */
public class testDevolverBilletes {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int elegirmonto;

        System.out.println("Billetes disponibles aquí:\tS/.20\tS/.50\tS/.100");
        do {
            System.out.println("Ingresar monto: ");
            elegirmonto = leer.nextInt();
        } while (elegirmonto < 0);

        devolverBilletes devbill = new devolverBilletes(elegirmonto);
        devbill.devolviendoBilletes(elegirmonto);
//         System.out.println("Entregando " + devbill.devolviendoCien(elegirmonto) + " billetes de 100");
//         System.out.println("Entregando " + devbill.devolviendoCincuenta(elegirmonto) + " billetes de 50");
//         System.out.println("Entregando " + devbill.devolviendoVeinte(elegirmonto) + " billetes de 20");
//        
        System.out.println("Usted ha retirado: " + devbill.retiroreal());
        System.out.println("(PRUEBA)Usted ha retirado: " + (devbill.retiroreal()+10));

        String asd = Integer.toString(devbill.retiroreal());
        int as = Integer.parseInt("20");
        System.out.println("(PRUEBA)Usted ha retirado: " + (devbill.retiroreal()+as));
        System.out.println("(PRUEBA)Usted ha retirado: " + (asd+10));
//         System.out.println("---- > " + (devbill.devolviendoCien(elegirmonto)));
//         System.out.println("---- > " + (devbill.devolviendoCincuenta(elegirmonto)));
//         System.out.println("---- > " + (devbill.devolviendoVeinte(elegirmonto)));
//         System.out.println("---- > " + (devbill.devolviendoCien(elegirmonto)));
//         System.out.println("---- > " + (devbill.devolviendoCincuenta(elegirmonto)));
//         System.out.println("---- > " + (devbill.devolviendoVeinte(elegirmonto)));
        System.out.println("(PRUEBA)---- > " + devbill.getCan100());
        System.out.println("(PRUEBA)---- > " + devbill.getCan50());
        System.out.println("(PRUEBA)---- > " + devbill.getCan20());
        System.out.println("(PRUEBA---- > " + devbill.getCan100());
        System.out.println("(PRUEBA)---- > " + devbill.getCan50());
        System.out.println("(PRUEBA)---- > " + devbill.getCan20());

    }
}
