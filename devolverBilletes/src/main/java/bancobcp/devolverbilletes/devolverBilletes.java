/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bancobcp.devolverbilletes;

/**
 *
 * @author Eduardo Durand <eduardo.durand.c@uni.pe>
 */
public class devolverBilletes {

    private int cantidad;
    private int can100 = 0, can50 = 0, can20 = 0;
    private int retiroverdadero;

    public devolverBilletes(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCan100() {
        return can100;
    }

    public int getCan50() {
        return can50;
    }

    public int getCan20() {
        return can20;
    }

    
    public void devolviendoBilletes(int cantidad) {
        while (cantidad >= 100) {
            can100++;
            cantidad = cantidad - 100;
        }

        while (cantidad >= 50) {
            can50++;
            cantidad = cantidad - 50;
        }

        while (cantidad >= 20) {
            can20++;
            cantidad = cantidad - 20;
        }

//        if (can100 > 1) {
//            System.out.println("Entregando " + can100 + " billetes de 100");
//        } else {
//            System.out.println("Entregando " + can100 + " billete de 100");
//        }
//
//        if (can50 > 1) {
//            System.out.println("Entregando " + can50 + " billetes de 50");
//        } else {
//            System.out.println("Entregando " + can50 + " billete de 50");
//        }
//
//        if (can20 > 1) {
//            System.out.println("Entregando " + can20 + " billetes de 20");
//        } else {
//            System.out.println("Entregando " + can20 + " billete de 20");
//        }
        
    }

//    public int devolviendoCien(int cantidad) {
//        while (cantidad >= 100) {
//            can100++;
//            cantidad = cantidad - 100;
//        }
//        return can100;
//    }
//
//    public int devolviendoCincuenta(int cantidad) {
//        while (cantidad >= 100) {
//            can100++;
//            cantidad = cantidad - 100;
//
//        }
//        while (cantidad >= 50) {
//            can50++;
//            cantidad = cantidad - 50;
//        }
//        return can50;
//    }
//
//    public int devolviendoVeinte(int cantidad) {
//        while (cantidad >= 100) {
//            can100++;
//            cantidad = cantidad - 100;
//
//        }
//        while (cantidad >= 50) {
//            can50++;
//            cantidad = cantidad - 50;
//        }
//        while (cantidad >= 20) {
//            can20++;
//            cantidad = cantidad - 20;
//        }
//        return can20;
//
//    }

    public int retiroreal() {
        this.retiroverdadero = this.can20 * 20 + this.can50 * 50 + this.can100 * 100;
        return retiroverdadero;
    }

}
