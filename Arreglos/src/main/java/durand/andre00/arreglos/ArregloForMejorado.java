/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package durand.andre00.arreglos;

/**
 *
 * @author Eduardo Durand <eduardo.durand.c@uni.pe>
 */
public class ArregloForMejorado {

    public static void main(String[] args) {
        int arreglo[] = {87, 68, 94, 100, 83, 78, 85, 91, 76, 87};
        int total = 0;
        for (int numero : arreglo) {
            total += numero;
        }
        System.out.println("La suma de los elementos del arreglo es: " + total);
        int[] array={6,2,4};
        arrayMystery(array);
        /*for(int i=0;i<array.length;i++){
            System.out.print(array[i] + ", ");
        }*/
        for(int valor:array){
            System.out.print(valor+", ");
        }
        int n;
        java.util.Scanner entrada = new java.util.Scanner(System.in);
        System.out.print("\nIngrese el tamaño de su arreglo: ");
        n = entrada.nextInt();
        int[] andre;
        andre = new int[n];
        
        for(int i=0;i<andre.length;i++){
            System.out.println("Ingrese el elemento andre["+i+"]: ");
            andre[i] = entrada.nextInt();
        }
        for(int valor:andre){
            System.out.print(valor+", ");
        }
    }

    public static void arrayMystery(int[] a) {
        for (int i = 1; i < a.length - 1; i++) {
            a[i] = a[i - 1] - a[i] + a[i + 1];
        }
    }
    
}
