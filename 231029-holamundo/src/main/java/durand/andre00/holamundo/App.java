/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package durand.andre00.holamundo;

import java.util.Scanner;

/**
 *
 * @author durandandre00 <durand.andre00@gmail.com>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        String nombre,apellido;
        char letra;
        int edad;
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese sus nombres: ");
        //nombre = entrada.next();
        nombre = entrada.nextLine();
        System.out.print("Ingrese sus apellidos: ");
        apellido = entrada.nextLine();
        System.out.print("Ingresse su edad: ");
        edad = entrada.nextInt();
        System.out.print("Ingrese su género masculino (M) o femenino (F): ");
        letra = entrada.next().charAt(0);
        System.out.println("SUS DATOS SON:");
        System.out.println("- Su nombre es: " + nombre);
        System.out.println("- Su apellido es: " + apellido);
        System.out.println("- Su eddad es: " + edad);
        System.out.println("- Su género es: " + letra);
        
    }
}
