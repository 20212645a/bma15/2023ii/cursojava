/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package durand.andre00.problemas;

/**
 *
 * @author Eduardo Durand <eduardo.durand.c@uni.pe>
 */
public class problema1 {

    public static void main(String[] args) {
        int a = 10;
        int b = 5;
        int c = 2;
        int result;
       
        result = ((a-- + b * c) / (++b - c) + (a++ * c)) % (a + (b-- * c) + (--c))+((a++ * b--) + (c++ - a) / (++b + c)) % (a-- * c++)+((a++ * b--) + (c++ - a) / (++b + c)) % (a-- * c++);
        System.out.println("La letra a es: " + a);
        System.out.println("La letra b es: " + b);
        System.out.println("La letra c es: " + c);
        System.out.println("El resultado es: " + result);
        
      
    }
    
}
