/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package durand.andre00.problemas;

/**
 *
 * @author durandandre00 <durand.andre00@gmail.com>
 */
public class Problemas {

    public static void main(String[] args) {
        System.out.println(" + INCREMENTO Y DECREMENTO + ");
        int x = 3;
        int y;
        y = ++x * 5 / x-- + --x;
        System.out.println("el valor de y es: " + y);
        System.out.println("el valor de x es:" + x);
        System.out.println(" + OPERADORES CON CORTO Y SIN CORTOCIRCUITO + ");
        System.out.println("CON CORTO (||)");
        int a8 = 6;
        boolean b8;
        b8 = (a8 >= 6) || (++a8 <= 7);
        //b8 = (++a8<=7) || (a8>=6);
        System.out.println("b8 es: " + b8);
        System.out.println("a8 es: " + a8);
        System.out.println("SIN CORTO (|)");
        int a9 = 6;
        boolean b9;
        b9 = (a9 >= 6) | (++a9 <= 7);
        System.out.println("b9 es: " + b9);
        System.out.println("a9 es: " + a9);
        
        

    }

}
