/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package durand.andre00.problemas;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Eduardo Durand <eduardo.durand.c@uni.pe>
 */
public class Ventana extends JFrame {

    public Ventana() {
        setSize(500, 500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Minecraft");
        setLocationRelativeTo(null);
        iniciarComponentes();
    }
    
    private void iniciarComponentes() {
        JPanel panel = new JPanel();
        //panel.setBackground(Color.green);
        this.getContentPane().add(panel);
    }
}
