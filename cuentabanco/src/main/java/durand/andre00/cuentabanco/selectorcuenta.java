/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package durand.andre00.cuentabanco;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Eduardo Durand <eduardo.durand.c@uni.pe>
 */
public class selectorcuenta extends JFrame {

    public selectorcuenta() {
        setSize(500, 500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Selecciona una de tus cuentas");
        setLocationRelativeTo(null);
        iniciarComponentes();
        
    }
    
    private void iniciarComponentes() {
        JPanel panel = new JPanel ();
        panel.setLayout(null);
        this.getContentPane().add(panel);
        
        JLabel etiqueta = new JLabel();
        etiqueta.setOpaque(true);
        etiqueta.setText ("Seleccione una de tus cuentas");
        etiqueta.setBounds(0, 0, 900, 12);
        etiqueta.setForeground(Color.WHITE);
        etiqueta.setBackground(Color.BLACK);
        panel.add(etiqueta);
    }
}
